#!/usr/bin/env fish

# Wallpaper
#swaybg -i ~/Pictures/wall-dark -m fill &
swaybg -i ~/Pictures/wall-light.jpg -m fill &

# Bar
waybar &

# Sound
pipewire &
pipewire-pulse &
wireplumber &

# Notifications
dunst &
~/Dotfiles/notifications/.notify/check_battery.fish &

# Automount USB and stuff
udiskie &

# Authentication
/usr/libexec/polkit-kde-authentication-agent-1 &
