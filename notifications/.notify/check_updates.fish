#!/usr/bin/env fish

while true
    # Try to update and install in dry-run mode.
    set ups (doas xbps-install -Sun | sed '/hold/d' | wc -l)

    # Send notification if there are any
    if test $ups -gt 0
        notify-send -i system-software-update "Info" "There are $ups updates available"
    end

    sleep 30m
end

