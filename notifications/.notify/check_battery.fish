#!/usr/bin/env fish

# A simple fish script that constantly checks the battery usage and sends 
# notifications when running under low power or when the device has been plugged in

# --- Variables --- #
set ICON_THEME  "Papirus-Dark"
set ICON_SIZE   "24x24@2x"
set ICON_PATH   "/usr/share/icons/$ICON_THEME/$ICON_SIZE/panel/battery"
set TIMER       60

set full_charge 0

# --- Main program --- #
while true
    set bat_charge (cat /sys/class/power_supply/BAT0/capacity)
    set bat_status (cat /sys/class/power_supply/BAT0/status)
    set bat_remaining  (acpi | awk '{ printf "%s", $5 }' | awk '{ split($0,a,":"); print a[1]"h "a[2]"m" }')

    switch $bat_status
        case "Discharging"
            if test $bat_charge -le 100; and test $full_charge -eq 1
                notify-send --urgency=normal --icon="$ICON_PATH-100.svg" "Info" "Power unplugged, discharging. $bat_remaining of charge left"
                set full_charge 0

            else if test $bat_charge -le 15
                notify-send --urgency=critical --icon="$ICON_PATH-000.svg" "Warning" "Battery is on $bat_charge%\n$bat_remaining of charge left"

            end

        case "Charging" "Full"
            if test $bat_charge -le 100; and test $bat_charge -gt 95; and test $full_charge -eq 0
                notify-send --urgency=normal --icon="$ICON_PATH-100.svg" "Info" "Charging finished!"
                set full_charge 1
            end
    end

    # Timer
    sleep $TIMER
end

