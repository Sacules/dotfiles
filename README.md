![clean dwm](/pics/clean.png "clean")

Personal dotfiles organized using GNU Stow for managing the symlinks - for instance, a simple 

```console
$ stow vim
```
automatically symlinks all the files under the `vim/` folder to the parent directory, so I keep this folder on my home like `~/dotfiles`.

Programs
------

![alt text](/pics/vim-ncmpcpp-ranger-dunst.png)

* **vim** for text editing and coding, with the lightline status bar and other plugins.
* **ncmpcpp** for music listening, with a script that sends a notification on each new song.
* **ranger**, the ultimate terminal file manager, with devicons and pdf, pictures, and video previews.
* **dunst** very lightweight and customizable notification daemon.

![alt text](/pics/neofetch-gotop-cava-cmatrix.png)
![alt text](/pics/bstack.png)

* **neofetch** for cool system info.
* **gotop** for monitoring resource usage - here i'm using it with the `--minimal` flag.
* **cava** for neat sound visualization.
* **cmatrix** not part of my dots, just here for that cool *hax0r* feel.
* **pipes.sh** for eye candy, with the `-t 6`flag.

![alt text](/pics/mpv-minifetch.png)

* **minifetch**, a minimal version of neofetch, with some things disabled.
* **mpv**, excellent video player for the terminal. Watching the *Cowboy Bebop* intro here.

![alt text](/pics/irssi.png)

* **irssi** for IRC, using a modified version of the *weed* theme to get rid of the timestamps.
