# Essays
- [ ] [Our Software Dependency Problem](https://research.swtch.com/deps), _Russ Cox_
- [ ] [Semantic Import Versioning (Go & Versioning, Part 3)](https://research.swtch.com/vgo-import), _Russ Cox_
- [ ] [Object Oriented Programming is an expensive disaster which must end](https://tinyurl.com/oopbad), _Lawrence Krubner_
- [ ] [What does dynamic linking and communism have got in common?](https://tinyurl.com/dlinkingbad), _Roman Shaposhnik_
- [X] [Reflections on Trusting Trust](http://genius.cat-v.org/ken-thompson/texts/trusting-trust/), _Ken Thompson_

# Articles
- [ ] [Vim mappings](https://vimways.org/2018/for-mappings-and-a-tutorial/)
- [ ] [Mastering Motion: The Journey to Emulate MotionPlus](https://dolphin-emu.org/blog/2019/04/26/mastering-motion/)
- [ ] [Try this vi setup to keep and organize your notes](https://opensource.com/article/18/6/vimwiki-gitlab-notes)

# Misc
- [ ] [Overwritten comment](https://pastebin.com/64GuVi2F/52966)
