
██╗    ██╗██╗██╗  ██╗██╗
██║    ██║██║██║ ██╔╝██║
██║ █╗ ██║██║█████╔╝ ██║
██║███╗██║██║██╔═██╗ ██║
╚███╔███╔╝██║██║  ██╗██║
 ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝

# Daily
[Todo](Todo)
[Uni](uni/index.md)

# Bookmarks
[Music](Music)
[Software](Software)
[Art](Art)
