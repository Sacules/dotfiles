# Libros
- [ ] Terminar
    - [ ] House of Leaves
    - [ ] Deathconsciousness

- [ ] Comenzar
    - [ ] Meditaciones, _Marco Aurelio_
    - [ ] Electronic, Dance Music, _Manu Gonzalez_

# Misc
- [ ] Ir al centro
    - [ ] Cambiar zapatillas
      - [ ] Limpiarlas
    - [ ] Pagar expensas
    - [ ] Pagar impuestos
    - [ ] Pagar alquiler
