* No existe el `or` en Prolog
* Hay que definir reglas consisas para trabajar alrededor de eso
* Ejemplo (Modus Ponem):

```prolog
hombre(Socrates).
mortal(hombre) :- mortal(Socrates).
```

* Con `!` (`cut`) sólo importa que se encuentre un resultado, hace que termine ahí la búsqueda
* Normalmente en producción se corta la búsqueda tempranamente, tomaría mucho sino
