# Objetivos
- Desarrollo de programas modulares
    - Refinamiento incremental
    - Separación entre interfaz, especificación, implementación

- Soporte para modularidad
    - Abstracción procedural
    - Tipos abstractos de datos
    - Paquetes y módulos
    - Abstracciones genéricas
