* Lenguajes de muy alto nivel
* El alcance suele ser global
* Los lenguajes se ampliaron para cubrir problemas más complejos
* Programadores vagos de aprender nuevos lenguajes
* Casteo a dos manos
* Decisiones de diseño muy diferentes - eficacia del programador sobre eficiencia
* La idea es escribir menos
* Asumen un usuario experto, que sabe lo que quiere hacer y el dominio de trabajo
* Seguridad de tipo, reglas de alcance, etc no son problema
* Robustez y prevenir bugs
* Permitir chanchadas - la culpa recae en el programador
* Bastante capaces de interactuar con otros procesos
* _Glue languages_
* Un propósito muy limitado y fijo
* Poca verbosidad
