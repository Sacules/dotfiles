-- Too lazy to repeat myself
local set = vim.opt
local g = vim.g

-- ┏━━━━━━━━━━━━━┓
-- ┃   General   ┃
-- ┗━━━━━━━━━━━━━┛

-- Wrap text in a nice way
set.wrap = true
set.linebreak = true

-- Traverse line breaks
set.whichwrap = "b,s,<,>,[,]"

-- Redraw only when necessary
set.lazyredraw = true

-- Allow more time to redraw for large files
set.redrawtime = 10000

-- Set tabs to have 4 spaces
set.tabstop = 4
set.softtabstop = 4

-- Expand tabs into spaces
set.expandtab = false

-- When using the >> or << commands, shift lines by 4 spaces
set.shiftwidth = 4

-- Show the matching part of the pair for [] {} and ()
set.showmatch = true

-- Always show at least one line below/above the cursor
set.scrolloff = 1
set.sidescrolloff = 5

-- Show changes in real time
set.inccommand = 'split'

-- Better searching
set.ignorecase = true
set.smartcase = true

-- Don't give completion messages like 'match 1 of 2'
-- or 'The only match'
-- set.shortmess = set.shortmess .. c

-- Enable mouse on all modes
set.mouse = 'a'

-- Hide default bar
set.showmode = false

-- Better autocomplete
set.completeopt = {'menu', 'menuone', 'noselect'}


-- ┏━━━━━━━━━━━━┓
-- ┃   Colors   ┃
-- ┗━━━━━━━━━━━━┛

-- Enable 24-bit RGB colors
set.termguicolors = true


-- ┏━━━━━━━━━━┓
-- ┃   Misc   ┃
-- ┗━━━━━━━━━━┛

-- Nicer vertical separators
set.fillchars = {
  horiz = '━',
  horizup = '┻',
  horizdown = '┳',
  vert = '┃',
  vertleft  = '┫',
  vertright = '┣',
  verthoriz = '╋',
}

-- Disable some builtin vim plugins
local default_plugins = {
  "2html_plugin",
  "getscript",
  "getscriptPlugin",
  "gzip",
  "logipat",
  "netrw",
  "netrwPlugin",
  "netrwSettings",
  "netrwFileHandlers",
  "matchit",
  "tar",
  "tarPlugin",
  "rrhelper",
  "spellfile_plugin",
  "vimball",
  "vimballPlugin",
  "zip",
  "zipPlugin",
  "tutor",
  "rplugin",
  "syntax",
  "synmenu",
  "optwin",
  "compiler",
  "bugreport",
  "ftplugin",
}

for _, plugin in pairs(default_plugins) do
  g["loaded_" .. plugin] = 1
end

-- additional filetypes
vim.filetype.add({
 extension = {
  templ = "templ",
 },
})
