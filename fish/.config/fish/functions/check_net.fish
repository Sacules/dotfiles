function check_net -d 'Gets network state and send a notification'
    # --- Variables --- #
    set ICON_THEME  "Papirus-Dark"
    set ICON_SIZE   "24x24@2x"
    set ICON_PATH   "/usr/share/icons/$ICON_THEME/$ICON_SIZE/panel/network"

    # --- Main program --- #
    set test_active_interface (ip addr show | grep "state UP")

    if test $status -eq 0
        set active_interface (echo $test_active_interface | awk '{printf "%s", substr($2,1,length($2)-1)}')

        switch $active_interface
            case "wlp3s0" "wlan0"
                if ping -c 1 8.8.8.8 > /dev/null 2>&1
                    set wifi_name (iwgetid -r)
                    notify-send --urgency=normal --icon="$ICON_PATH-wireless-signal-excellent.svg" \
                    "Wi-Fi" "Connected to $wifi_name"
                else
                    notify-send --urgency=normal --icon="$ICON_PATH-offline.svg" \
                    "Wi-Fi" "No connection available"
                end
            case "enp025"
                notify-send --urgency=normal --icon="$ICON_PATH-wired.svg" \
                "Info" "Connected to wired network"
        end

    else
        notify-send --urgency=normal --icon="$ICON_PATH-offline.svg" \
        "Info" "No connection available"
    end
end
