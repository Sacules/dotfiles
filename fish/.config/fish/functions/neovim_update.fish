function neovim_update
	echo "$bold Downloading latest neovim..."
    rm -v ~/Downloads/nvim.appimage
    aria2c --dir=$HOME/Downloads 'https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage'
    chmod +x ~/Downloads/nvim.appimage
end
