#!/usr/bin/env fish

function check_bat -d 'Get battery usage and send a notification'
    # --- Variables --- #
    set ICON_THEME  "Papirus-Dark"
    set ICON_SIZE   "24x24@2x"
    set ICON_PATH   "/usr/share/icons/$ICON_THEME/$ICON_SIZE/panel/battery"

    # --- Main program --- #
    set bat_charge (cat /sys/class/power_supply/BAT0/capacity)
    set bat_status (cat /sys/class/power_supply/BAT0/status)
    set bat_remaining  (acpi | awk '{ printf "%s", $5 }' | awk '{ split($0,a,":"); print a[1]"h "a[2]"m" }')

    switch $bat_status
        case "Discharging"
            if test $bat_charge -le 100
                notify-send --urgency=normal --icon="$ICON_PATH-100.svg" \
                "$bat_status" "$bat_remaining of charge left"

            else if test $bat_charge -le 15
                notify-send --urgency=critical --icon="$ICON_PATH-000.svg" \
                "Warning" "Battery is on $bat_charge%\n$bat_remaining of charge left"

            end
    end
end
